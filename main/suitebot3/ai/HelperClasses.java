package suitebot3.ai;


import java.awt.List;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import suitebot3.game.Action;
import suitebot3.game.Agent;
import suitebot3.game.Bomb;
import suitebot3.game.FieldState;
import suitebot3.game.GamePlan;
import suitebot3.game.GameState;
import suitebot3.game.GameState.FieldPos;
import suitebot3.game.Point;
import suitebot3.game.UpdatableFieldState;

public class HelperClasses {

	
	public static Point countDistanceBetweenPoints(GamePlan gameplan, Point p1 ,Point p2) {
		int xDistance = Math.abs(p1.x- p2.x);
		int yDistance = Math.abs(p1.y- p2.y);
		
		if(xDistance > gameplan.getWidth()/2) {
			//TODO
			xDistance = gameplan.getWidth() - Math.max(p1.x, p2.x);
			xDistance += Math.min(p1.x, p2.x);
		}
		
		if(yDistance > gameplan.getHeight()/2) {
			yDistance = gameplan.getHeight() - Math.max(p1.y, p2.y);
			yDistance +=Math.min(p1.y, p2.y);
		}
		
		
		
		return new Point(xDistance, yDistance);
	}
	
	public static Point direction(GamePlan gameplan, Point start ,Point end) {
		
		Point distance =  countDistanceBetweenPoints(gameplan, start, end);
		
		int xDistance =distance.x;
		int yDistance = distance.y;
		
		int xRes = distance.x;
		int yRes = distance.y;
		
		
		if (start.x > end.x   &&  Math.abs(start.x-end.x) <= xDistance )  {
			xRes = -distance.x;
		}else if (start.x < end.x &&  Math.abs(start.x-end.x) > xDistance )  {
			xRes = -distance.x;
		}
		
		if (start.y > end.y   &&  Math.abs(start.y-end.y) <= yDistance )  {
			yRes = -distance.y;
		}else if (start.y < end.y &&  Math.abs(start.y-end.y) > yDistance )  {
			yRes = -distance.y;
		}
		return new Point(xRes, yRes);
	}
	
	
	public static boolean isSafePlaceFromBomb (GameState gs, Point p ) {
		
		
		java.util.List<FieldPos> positions = gs.allFields().collect(Collectors.toList());

		for (FieldPos fieldPos2 : positions) {
			FieldState field  =gs.getField(fieldPos2.position);
			if(field.getBomb() == null) {
				continue;
			}

			Bomb bomb =field.getBomb();
			
			Point dist = HelperClasses.countDistanceBetweenPoints(gs.getGamePlan(), p, fieldPos2.position);
			if(dist.absolutDistance() >3) {
				continue;
			}
			
			if(dist.absolutDistance() <=2 ) {
				return false;
			}
			
			if(dist.x == 0 || dist.y == 0) {
				return false;
			}			
			
		}	
	
		
		return true;
		
	}

	
	
public static HashSet<Point> findSafePointToRun(GameState gs, Point player) {
	//there is bomb 
	java.util.List<FieldPos> positions = gs.allFields().collect(Collectors.toList());
	Map<Bomb,Point> bombs = new HashMap<>();
	Map<Agent,Point> players = new HashMap<>();
	
	
	for (FieldPos fieldPos2 : positions) {
		FieldState field  =gs.getField(fieldPos2.position);
		
		if(field.getBomb() != null) {
			bombs.put(field.getBomb(), fieldPos2.position);
		}
		if(field.getAgent() != null) {
			players.put(field.getAgent(), fieldPos2.position);
		}
	}
	
	HashSet<Point> avaioablePoints = new HashSet<>();
	HashSet<Point> badPoints = new HashSet<>();
	for (Bomb b : bombs.keySet()) { 
		if (!isSafePlaceFromBomb ( gs, player, bombs.get(b))) {
			int  stepsRemaining = b.getCountdown();
			
			for (int i =player.x - stepsRemaining; i <= player.x + stepsRemaining; i++) {
				
				for (int j =player.y - stepsRemaining; j <= player.y + stepsRemaining; j++) {
					
					int xPos =  (gs.getGamePlan().getWidth()+i ) %  gs.getGamePlan().getWidth();
					int yPos =  (gs.getGamePlan().getHeight()+j ) %  gs.getGamePlan().getHeight();
					Point testedPoint = new Point(xPos, yPos);
					if (isSafePlaceFromBomb(gs,testedPoint, bombs.get(b)) ) {
						if(!badPoints.contains(testedPoint)) {
							avaioablePoints.add(testedPoint);
						}
					}else {
						if(avaioablePoints.contains(testedPoint)) {
							avaioablePoints.remove(testedPoint);
						}
						badPoints.add(testedPoint);
					}
				}
				
			}
			
		}
	}
	return avaioablePoints;
	
}
	
//need to place a bomb
public static boolean ImminentThread (GameState gs, Point player) {

	java.util.List<FieldPos> positions = gs.allFields().collect(Collectors.toList());
	Map<Bomb,Point> bombs = new HashMap<>();
	Map<Agent,Point> players = new HashMap<>();
	
	
	for (FieldPos fieldPos2 : positions) {
		FieldState field  =gs.getField(fieldPos2.position);
		
		if(field.getBomb() != null) {
			bombs.put(field.getBomb(), fieldPos2.position);
		}
		if(field.getAgent() != null) {
			players.put(field.getAgent(), fieldPos2.position);
		}
	}
	for (Bomb b : bombs.keySet()) {
		if (!isSafePlaceFromBomb ( gs, player, bombs.get(b))) {
			if (b.getCountdown() == 0) {
				return true;
			}else if (b.getCountdown() == 1 && countDistanceBetweenPoints(gs.getGamePlan(), player, bombs.get(b)).absolutDistance() <=1)  {
		    return true;
				
			}
		}
	}
	
	for (Agent a : players.keySet()) {
		if (countDistanceBetweenPoints(gs.getGamePlan(), player, players.get(a)).absolutDistance() <2 && countDistanceBetweenPoints(gs.getGamePlan(), player, players.get(a)).absolutDistance() >0)  {
			return true;
		}
	}
	
	return false;
}


public static boolean isSafePlaceFromBomb (GameState gs, Point p, Point bombPoint ) {
		
		
		java.util.List<FieldPos> positions = gs.allFields().collect(Collectors.toList());

		//for (FieldPos fieldPos2 : positions) {
			FieldState field  =gs.getField(bombPoint);
			if(field.getBomb()== null) {
				return false;
			}

			Bomb bomb =field.getBomb();
			
			Point dist = HelperClasses.countDistanceBetweenPoints(gs.getGamePlan(), p, bombPoint);
			
			
			if(dist.absolutDistance() <=2 ) {
				return false;
			}
			if(dist.absolutDistance() ==3 && (dist.x == 0 || dist.y == 0)) {
				return false;
			}		
			
	//}	
	
		
		return true;
		
	}
	
//return if there is safe place null need to run
public static Action chooseSafeAround(GameState gs, Point p ) {
		
		 Calendar cal = Calendar.getInstance();
		Random random = new Random(cal.getTimeInMillis() );
		
		ArrayList<Action> availableAction= new ArrayList<>();
		Point up = Action.UP.from(p);
		up = new Point(up.x, (up.y+ gs.getGamePlan().getHeight()) % gs.getGamePlan().getHeight());
	
		if(isSafePlaceFromBomb (gs, up )) {
			availableAction.add(Action.UP);
		}
		
		
		
		Point down = Action.DOWN.from(p);
		down = new Point(down.x,( down.y+ gs.getGamePlan().getHeight()) % gs.getGamePlan().getHeight()); 
		
		if(isSafePlaceFromBomb (gs,  down )) {
			availableAction.add(Action.DOWN);
		}
		
		Point left = Action.LEFT.from(p);
		left = new Point((left.x + gs.getGamePlan().getWidth()) % gs.getGamePlan().getWidth(), left.y ); 
		
		if(isSafePlaceFromBomb (gs, left )) {
			availableAction.add(Action.LEFT);
		}
		
		
		Point right = Action.RIGHT.from(p);
		right = new Point((right.x + gs.getGamePlan().getWidth()) % gs.getGamePlan().getWidth(), right.y); 
		
		if(isSafePlaceFromBomb (gs, right )) {
			availableAction.add(Action.RIGHT);
		}
		
		if(availableAction.size() == 0) {
			return null;
		}
		return availableAction.get(random.nextInt(availableAction.size()));
	}

	public static Action[] chooseBestMovepatern(GameState gs,ArrayList<Action[]> patterns, Point p ) {
		Action[] bestAction = null;
		int bestRescource = -1;
		Point testedPoin =null;
		for (Action[] a : patterns ) {
			 Point futureP = SampleBotAi.FuturePoint(a);
			 testedPoin = p.plus(futureP);
			 testedPoin= new Point((testedPoin.x + gs.getGamePlan().getWidth()) % gs.getGamePlan().getWidth(), ( testedPoin.y+ gs.getGamePlan().getHeight()) % gs.getGamePlan().getHeight());
			 if(isSafePlaceFromBomb(gs, testedPoin)) {
				int rescource =  bombScore(gs,testedPoin);
				if(rescource > bestRescource) {
					bestRescource = rescource;
					bestAction=a;
				}
			 }
		}
		
		return bestAction;
	}
	
	public static int bombScore(GameState gs, Point bombPoint ) {
	  int score = 0;
		for (int i =bombPoint.x - 3; i <= bombPoint.x + 3; i++) {
			
			for (int j =bombPoint.y - 3; j <= bombPoint.y + 3; j++) {
				
				int xPos =  (gs.getGamePlan().getWidth()+i ) %  gs.getGamePlan().getWidth();
				int yPos =  (gs.getGamePlan().getHeight()+j ) %  gs.getGamePlan().getHeight();
				Point testedPoint = new Point(xPos, yPos);
				int absDIstance = countDistanceBetweenPoints(gs.getGamePlan(), bombPoint, testedPoint).absolutDistance();
				if(absDIstance==3) {
					if(bombPoint.x == testedPoint.x || bombPoint.y == testedPoint.y) {
						score+= gs.getField(testedPoint).getResourceCount();
					}
				}else if (absDIstance <=2) {
					score+= gs.getField(testedPoint).getResourceCount();
				}
			}
			
		}
		return  score;
	}

}
