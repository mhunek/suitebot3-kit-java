package suitebot3.ai;

import suitebot3.game.Action;
import suitebot3.game.GameState;

public interface BotAi
{
	/**
	 * @param gameState - the current state of the game
	 * @return the moves to play
	 */
	Action makeMove(GameState gameState);
}
