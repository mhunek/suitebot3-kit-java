package suitebot3.ai;

import suitebot3.game.Action;
import suitebot3.game.Agent;
import suitebot3.game.GameSetup;
import suitebot3.game.GameState;
import suitebot3.game.Point;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.swing.text.Position;

public class SampleBotAi implements BotAi
{
	private int myId;
	private Random random = new Random();

	
	public static Action[] ACTIONS1 = {	Action.UP,	Action.LEFT,	Action.UP};
	public static Action[] ACTIONS2 = {	Action.UP,	Action.RIGHT,	Action.UP};
	public static Action[] ACTIONS3 = {	Action.DOWN,	Action.LEFT,	Action.DOWN};
	public static Action[] ACTIONS4  = {	Action.DOWN,	Action.RIGHT,	Action.DOWN};
	public static Action[] ACTIONS5  = {	Action.UP,	Action.UP,	Action.UP,Action.UP};	
	public static Action[] ACTIONS6 = {	Action.DOWN,	Action.DOWN,	Action.DOWN,Action.DOWN};
	public static Action[] ACTIONS7 = {	Action.UP,	Action.LEFT,	Action.UP,Action.LEFT};
	public static Action[] ACTIONS8 = {	Action.UP,	Action.RIGHT,	Action.UP, Action.RIGHT};
	public static Action[] ACTIONS9 = {	Action.DOWN,	Action.LEFT,	Action.DOWN, Action.LEFT};
	public static Action[] ACTIONS10  = {	Action.DOWN,	Action.RIGHT,	Action.DOWN,Action.RIGHT};
	public static Action[] ACTIONS11 = {	Action.UP,	Action.LEFT,	Action.UP,Action.UP};
	public static Action[] ACTIONS12 = {	Action.UP,	Action.RIGHT,	Action.UP, Action.UP};
	public static Action[] ACTIONS13 = {	Action.DOWN,	Action.LEFT,	Action.DOWN, Action.DOWN};
	public static Action[] ACTIONS14  = {	Action.DOWN,	Action.RIGHT,	Action.DOWN,Action.DOWN};
	
	public static Action ACTIONS[] = {
			Action.UP, Action.DOWN, Action.LEFT, Action.RIGHT,
			Action.UP, Action.LEFT, Action.UP, Action.LEFT,
			Action.PLANT_BOMB
	};
	private static ArrayList<Action> moves = new ArrayList<>();

	public SampleBotAi(GameSetup gameSetup)
	{
		myId = gameSetup.aiPlayerId;
	}

	@Override
	public Action makeMove(GameState gameState)
	{
		Agent agent = gameState.getAgentOfPlayer(myId);

		if(gameState.getCurrentRound() ==1) {
			return Action.PLANT_BOMB;
		}
		
		if(HelperClasses.ImminentThread(gameState, agent.getPosition())) {
			return Action.PLANT_BOMB; 
		}
		
		if(moves.isEmpty()) {
			
			findNextMoveSet(gameState, agent);
			if(HelperClasses.bombScore(gameState,agent.getPosition())!= 0) {
				return Action.PLANT_BOMB;
			}			
		}
		Action next = moves.get(0);		
		moves.remove(0);
		
		/*if ( HelperClasses.ImminentThread(gameState, nextStep(gameState,agent.getPosition(), next))) {
			
			if(HelperClasses.bombScore(gameState,agent.getPosition())!= 0) {
				return Action.PLANT_BOMB;
			}else {
				Action resultA = HelperClasses.chooseSafeAround(gameState,agent.getPosition());
				if(resultA != null) {
					return resultA;
				}
			}
			findNextMoveSet(gameState, agent);
			return Action.PLANT_BOMB;
		}*/
		if (agent != null)
			return next;

		return ACTIONS[1];
	}

	private void findNextMoveSet(GameState gameState, Agent agent) {
		ArrayList<Action[]> all = new ArrayList<Action[]>() ;
		all.add(SampleBotAi.ACTIONS1);
		all.add(SampleBotAi.ACTIONS2);
		all.add(SampleBotAi.ACTIONS3);
		all.add(SampleBotAi.ACTIONS4);
		all.add(SampleBotAi.ACTIONS5);
		all.add(SampleBotAi.ACTIONS6);
		all.add(SampleBotAi.ACTIONS7);
		all.add(SampleBotAi.ACTIONS8);
		all.add(SampleBotAi.ACTIONS9);
		all.add(SampleBotAi.ACTIONS10);
		all.add(SampleBotAi.ACTIONS11);
		all.add(SampleBotAi.ACTIONS12);
		all.add(SampleBotAi.ACTIONS13);
		all.add(SampleBotAi.ACTIONS14);
		Action[] result  =HelperClasses.chooseBestMovepatern(gameState,all,agent.getPosition() );
		moves = new ArrayList<Action>();
		for (Action a : result) {
			moves.add(a);
		}
	}
	
	
	
	
	public static Point FuturePoint (Action[] moves) {
		Point p = new Point(0, 0);
		for (Action action : moves) {
			p =p.plus(action.from( new Point(0, 0)));
		}
		return p;
	}
	
	public static Point nextStep (GameState gs, Point playerPos, Action next) {
		
		Point testedPoin = playerPos.plus(next.from(new Point(0,0)));
		testedPoin= new Point((testedPoin.x + gs.getGamePlan().getWidth()) % gs.getGamePlan().getWidth(), ( testedPoin.y+ gs.getGamePlan().getHeight()) % gs.getGamePlan().getHeight());
	
		return testedPoin;
	}

}
