package suitebot3.game;

import suitebot3.game.dto.BombDTO;

public class Bomb
{

    private Player owner;
    private int countdown;

    public Bomb(Player owner, int countdown)
    {
        this.owner = owner;
        this.countdown = countdown;
    }

    public Bomb(Bomb bomb)
    {
        this.owner = bomb.owner;
        this.countdown = bomb.countdown;
    }

    /** The owner of the bomb */
    public Player getPlayer()
    {
        return owner;
    }

    /**
     * Number of rounds remaining before the bomb explodes.
     * The bomb explodes in the round in which the countdown is 0.
     * During round evaluation all agents move and then bombs explode
     * so when countdown is 0 your agents have still one move to make.
     */
    public int getCountdown()
    {
        return countdown;
    }

    public void setCountdown(int countdown)
    {
        this.countdown = countdown;
    }


    public BombDTO toDto()
    {
        BombDTO dto = new BombDTO();

        dto.countdown = countdown;
        dto.ownerId = owner.id;

        return dto;
    }
}
