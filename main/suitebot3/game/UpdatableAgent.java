package suitebot3.game;

public interface UpdatableAgent extends Agent
{
    void setPosition(Point position);

    void setLastMove(Action lastMove);

    /** Kills the agent removes it from the player's state. */
    void kill();
}
