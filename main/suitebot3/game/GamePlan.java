package suitebot3.game;

import java.util.Map;

public interface GamePlan
{
	int getWidth();
	int getHeight();

	/** Returns how much resources the field will produce each round. */
	int getGrowRate(Point position);

	/** Returns the maximum number of resources the field can have. */
	int getMaxResources(Point position);
	
	/** Returns the number of resources the field has at the beginning of the game */
	int getInitialResources(Point position);

	/** Throws PointOutsideOfGamePlan exception if the position is not withing dimensions of the game plan. */
	void checkIsOnPlan(Point position);

	/** Map of home base positions. The key to the map is the player id. */
	Map<Integer, Point> getHomeBasePositions();

	/** Maximum number of rounds in a game. */
	int getMaxRounds();

	class PointOutsideOfGamePlan extends RuntimeException
	{
		public PointOutsideOfGamePlan(Point point)
		{
			super("Point " + point + " is outside of game plan");
		}
	}
}
