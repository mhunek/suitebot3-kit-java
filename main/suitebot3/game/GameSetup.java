package suitebot3.game;

import suitebot3.game.dto.GameStateDTO;

import java.util.List;

import static suitebot3.game.GameStateCreator.createGamePlanFromDto;
import static suitebot3.game.GameStateCreator.createPlayerListFromDto;

public class GameSetup
{
	public final int aiPlayerId;
	public final List<Player> players;
	public final GamePlan gamePlan;

	public GameSetup(int aiPlayerId, List<Player> players, GamePlan gamePlan)
	{
		this.aiPlayerId = aiPlayerId;
		this.players = players;
		this.gamePlan = gamePlan;
	}

	public static GameSetup fromGameStateDto(GameStateDTO dto)
	{
		return new GameSetup(dto.aiPlayerId, createPlayerListFromDto(dto), createGamePlanFromDto(dto));
	}
}
