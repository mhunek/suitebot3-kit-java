package suitebot3.game.dto;

import suitebot3.game.Action;
import suitebot3.game.Point;

/**
 * Do not modify.
 */
public class PlayerDTO
{
    public String name;
    public int resources;
    public Point homeBaseLocation;
    public Point agentLocation;
    public Action agentLastMove;
}
