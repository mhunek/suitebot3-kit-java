package suitebot3.game.dto;

import java.util.List;
import java.util.Map;

/**
 * Do not modify.
 */
public class GameStateDTO
{
    public int aiPlayerId;

    public int currentRound;
    public int remainingRounds;

    public int gamePlanWidth;
    public int gamePlanHeight;

    public Map<Integer, PlayerDTO> players;
    public List<List<Integer>> fieldResources;
    public List<List<BombDTO>> fieldBombs;
}