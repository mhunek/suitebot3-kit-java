package suitebot3.game.util;

import suitebot3.game.GameConstants;

public class VisualisationUtil
{
    public static final String RESOURCES_CHARS = " .";
    public static final String BASE_CHARS = "ABCDEFGH";
    public static final String AGENT_CHARS = "abcdefgh";

    public static boolean isResourceChar(char ch)
    {
        return RESOURCES_CHARS.indexOf(ch) > -1;
    }

    public static char getResourceChar(int resourceCount)
    {
        int resourceCharIndex = RESOURCES_CHARS.length() * resourceCount / (GameConstants.FIELD_MAX_RESOURCES + 1);
        return RESOURCES_CHARS.charAt(resourceCharIndex);
    }

    public static int getResources(char ch)
    {
        int index = RESOURCES_CHARS.indexOf(ch);
        if (index < 0)
            throw new RuntimeException("'" + ch + "' is not a valid character for resources. Supported: " + RESOURCES_CHARS);

        return (int) Math.ceil(index * (double)GameConstants.FIELD_MAX_RESOURCES / RESOURCES_CHARS.length());
    }

    public static boolean isBaseChar(char ch)
    {
        return BASE_CHARS.indexOf(ch) > -1;
    }

    public static boolean isBombChar(char ch)
    {
        return Character.isDigit(ch);
    }

    public static int getBombCountdown(char ch)
    {
        return Integer.parseInt("" + ch);
    }

    public static char getBaseChar(int playerId)
    {
        return BASE_CHARS.charAt(playerId - 1);
    }

    public static boolean isAgentChar(char ch)
    {
        return AGENT_CHARS.indexOf(ch) > -1;
    }

    public static char getAgentChar(int playerId)
    {
        return AGENT_CHARS.charAt(playerId - 1);
    }

    public static int getPlayerId(char ch)
    {
        int id = AGENT_CHARS.indexOf(ch);
        if (id > -1)
            return id + 1;
        else
            return BASE_CHARS.indexOf(ch) + 1;
    }

}
