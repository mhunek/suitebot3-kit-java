package suitebot3.game;

public interface Agent
{
    /** The owner of the agent */
    Player getPlayer();

    /** Current position of the agent */
    Point getPosition();

    /** Returns the last action the agent did or null. */
    Action getLastMove();
}
