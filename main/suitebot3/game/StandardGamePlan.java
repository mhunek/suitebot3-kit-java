package suitebot3.game;

import com.google.common.base.Objects;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StandardGamePlan implements GamePlan
{
	private int width;
	private int height;
	private Map<Integer, Point> startingPositions;
	private int maxRounds;
	private final int[][] growRates;
	private final int[][] maxResources;
	private final int[][] initialResources;

	public StandardGamePlan(int width, int height, Map<Integer, Point> startingPositions)
	{
		this(width, height, startingPositions, GameConstants.ROUND_COUNT);
	}

	public StandardGamePlan(int width, int height, Map<Integer, Point> startingPositions, int maxRounds)
	{
		this.width = width;
		this.height = height;
		this.startingPositions = new HashMap<>(startingPositions);
		this.maxRounds = maxRounds;

		validateStartingPositions();

		growRates = new int[width][height];
		maxResources = new int[width][height];
		initialResources = new int[width][height];

		for (int x = 0; x < width; ++x) {
			for (int y = 0; y < height; ++y) {
				growRates[x][y] = GameConstants.GROW_RATE;
				maxResources[x][y] = GameConstants.FIELD_MAX_RESOURCES;
				initialResources[x][y] = GameConstants.FIELD_INITIAL_RESOURCES;
			}
		}
	}

	public StandardGamePlan(GamePlan gamePlan)
	{
		width = gamePlan.getWidth();
		height = gamePlan.getHeight();
		startingPositions = new HashMap<>(gamePlan.getHomeBasePositions());
		maxRounds = gamePlan.getMaxRounds();

		growRates = new int[width][height];
		maxResources = new int[width][height];
		initialResources = new int[width][height];

		for (int x = 0; x < width; ++x) {
			for (int y = 0; y < height; ++y) {
				growRates[x][y] = gamePlan.getGrowRate(new Point(x, y));
				maxResources[x][y] = gamePlan.getMaxResources(new Point(x, y));
				initialResources[x][y] = gamePlan.getInitialResources(new Point(x, y));
			}
		}
	}
	
	public StandardGamePlan(int width, int height, Map<Integer, Point> startingPositions, int maxRounds, List<List<Integer>> fieldInitialResources, List<List<Integer>> fieldMaxResources, List<List<Integer>> fieldGrowRates)
	{
		this(width, height, startingPositions, maxRounds);
		
		for (int x = 0; x < width; ++x) {
			for (int y = 0; y < height; ++y) {
				growRates[x][y] = fieldGrowRates.get(x).get(y);
				maxResources[x][y] = fieldMaxResources.get(x).get(y);
				initialResources[x][y] = fieldInitialResources.get(x).get(y);
			}
		}
	}
	
	private void validateStartingPositions()
	{
		for (Point position : startingPositions.values())
		{
			if (isOutsideOfPlan(position))
				throw new IllegalStateException("starting position is outside of the plan: " + position);
		}
	}

	@Override
	public void checkIsOnPlan(Point position)
	{
		if (isOutsideOfPlan(position))
			throw new PointOutsideOfGamePlan(position);
	}

	@Override
	public int getWidth()
	{
		return width;
	}

	@Override
	public int getHeight()
	{
		return height;
	}

	@Override
	public int getGrowRate(Point position)
	{
		checkIsOnPlan(position);
		return growRates[position.x][position.y];
	}
	
	public void setGrowRates(Point position, int value)
	{
		checkIsOnPlan(position);
		growRates[position.x][position.y] = value;
	}

	@Override
	public int getMaxResources(Point position)
	{
		checkIsOnPlan(position);
		return maxResources[position.x][position.y];
	}
	
	public void setMaxResources(Point position, int value)
	{
		checkIsOnPlan(position);
		maxResources[position.x][position.y] = value;
	}
	
	@Override
	public int getInitialResources(Point position)
	{
		checkIsOnPlan(position);
		return initialResources[position.x][position.y];
	}
	
	public void setInitialResources(Point position, int value)
	{
		checkIsOnPlan(position);
		initialResources[position.x][position.y] = value;
	}
	
	@Override
	public Map<Integer, Point> getHomeBasePositions()
	{
		return startingPositions;
	}

	@Override
	public int getMaxRounds()
	{
		return maxRounds;
	}

	private boolean isOutsideOfPlan(Point position)
	{
		return position.x < 0 || position.y < 0 || position.x >= width || position.y >= height;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		StandardGamePlan gamePlan = (StandardGamePlan) o;
		return width == gamePlan.width &&
				height == gamePlan.height &&
				maxRounds == gamePlan.maxRounds &&
				Objects.equal(startingPositions, gamePlan.startingPositions) &&
				Arrays.deepEquals(growRates, gamePlan.growRates) &&
				Arrays.deepEquals(maxResources, gamePlan.maxResources) &&
				Arrays.deepEquals(initialResources, gamePlan.initialResources);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(width, height, startingPositions, maxRounds, growRates, maxResources, initialResources);
	}
}
