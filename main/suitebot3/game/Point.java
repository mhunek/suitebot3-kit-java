package suitebot3.game;

import com.google.gson.annotations.Expose;

public class Point
{
	@Expose public final int x;
	@Expose public final int y;

	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public Point plus(Point other)
	{
		return new Point(x + other.x, y + other.y);
	}
	
	public Point plus(int x, int y)
	{
		return new Point(this.x + x, this.y + y);
	}
	
	public Point minus(Point other)
	{
		return new Point(x - other.x, y - other.y);
	}
	
	public Point minus(int x, int y)
	{
		return new Point(this.x - x, this.y - y);
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Point point = (Point) o;

		return (x == point.x && y == point.y);
	}
	
	public int absolutDistance() {
		return Math.abs(x)+Math.abs(y);
	}

	@Override
	public int hashCode()
	{
		int result = x;
		result = 31 * result + y;
		return result;
	}

	@Override
	public String toString()
	{
		return "[" + x + ", " + y + ']';
	}
}
