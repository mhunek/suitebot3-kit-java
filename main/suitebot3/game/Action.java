package suitebot3.game;

public enum Action
{
	UP(0, -1, "U"),
	DOWN(0, 1, "D"),
	LEFT(-1, 0, "L"),
	RIGHT(1, 0, "R"),
	HOLD(0, 0, "H"),
	PLANT_BOMB(0, 0, "B");

	public final int dx;
	public final int dy;
	public final String asString;

	Action(int dx, int dy, String asString)
	{
		this.dx = dx;
		this.dy = dy;
		this.asString = asString;
	}

	public Point from(Point source)
	{
		return new Point(source.x + dx, source.y + dy);
	}

	@Override
	public String toString()
	{
		return asString;
	}
}
