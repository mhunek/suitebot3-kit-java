package suitebot3.game;

import suitebot3.game.dto.BombDTO;
import suitebot3.game.dto.GameStateDTO;
import suitebot3.game.dto.PlayerDTO;
import suitebot3.game.util.VisualisationUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GameStateCreator
{

	/**
	 * Creates a game state from a string representation.
	 * See VisualisationUtil.java for description of all available characters.
	 * In general:
	 *  - Capital letters A-H are locations of home bases. Agents are placed on the home bases.
	 *  - Characters " ." are fields with different number of resources on it.
	 *    ' '  has 0 resources,
	 *    '.'  has 1 resource
	 *   - Numbers 1-9 are bombs belonging to player A with countdown equal to the number.
     *     There are 0 resources under a bomb.
	 *
	 * For example the following will create 5x5 game plan.
	 * There will be:
	 *  - 1 agent of player 1 (letter 'A')
	 *  - 1 agent of player 2 in the home base 'B'
	 *  - resources in the upper left corner and along the bottom
     *  - bomb with countdown 2 in the center belonging to player A
	 *
	 *     "...  \n" +
	 *     ".A   \n" +
	 *     "  2  \n" +
	 *     "  B  \n" +
	 *     ".....\n" +
	 */
	public static StandardGameState fromString(String gameStateAsString)
	{
		List<Player> players = new ArrayList<>();
		Map<Integer, Point> startingPositions = new HashMap<>();

		String[] lines = gameStateAsString.replaceAll("\n$", "").split("\n");
		assertRectangularPlan(lines);

		int width = lines[0].length();
		int height = lines.length;

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				char ch = lines[y].charAt(x);

				if (VisualisationUtil.isBaseChar(ch))
				{
					int playerId = VisualisationUtil.getPlayerId(ch);
					players.add(new Player(playerId, "Player " + playerId));
					startingPositions.put(playerId, new Point(x, y));
				}
				else if (!VisualisationUtil.isAgentChar(ch) && !VisualisationUtil.isResourceChar(ch) && !VisualisationUtil.isBombChar(ch))
					throw new GameStateCreationException("unrecognized character: " + ch);
			}
		}

		StandardGamePlan gamePlan = new StandardGamePlan(width, height, startingPositions);
		StandardGameState gameState = new StandardGameState(gamePlan, players, true);

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				char ch = lines[y].charAt(x);
				Point position = new Point(x, y);

				if (VisualisationUtil.isAgentChar(ch))
				{
					gameState.spawnAgent(position, VisualisationUtil.getPlayerId(ch));

					gameState.setResourcesOnField(position, 0);
				}
				else if (VisualisationUtil.isResourceChar(ch))
					gameState.setResourcesOnField(position, VisualisationUtil.getResources(ch));
				else if (VisualisationUtil.isBombChar(ch))
				{
				    Player playerA = gameState.getPlayerState(VisualisationUtil.getPlayerId('A')).getPlayer();
				    Bomb bomb = new Bomb(playerA, VisualisationUtil.getBombCountdown(ch));
                    gameState.setBombOnField(position, bomb);
                    gameState.setResourcesOnField(position, 0);
                }
				else if (!VisualisationUtil.isBaseChar(ch))
					throw new GameStateCreationException("unrecognized character: " + ch);
			}
		}

		return gameState;
	}

	private static void assertRectangularPlan(String[] lines)
	{
		int width = lines[0].length();

		for (int i = 1; i < lines.length; i++)
		{
			if (lines[i].length() != width)
			{
				throw new GameStateCreationException(
						String.format("non-rectangular plan: line %d width (%d) is different from the line 1 width (%d)",
								(i + 1), lines[i].length(), width));
			}
		}
	}

	public static StandardGameState fromDto(GameStateDTO dto)
	{
		List<Player> players = createPlayerListFromDto(dto);
		Map<Integer, Player> playerIdMap = new HashMap<>(players.size());
		for (Player player : players)
			playerIdMap.put(player.id, player);

		GamePlan gamePlan = createGamePlanFromDto(dto);

		StandardGameState gameState = new StandardGameState(gamePlan, players, dto.currentRound, dto.remainingRounds);

		for (Map.Entry<Integer, PlayerDTO> player : dto.players.entrySet())
		{
			PlayerDTO playerDTO = player.getValue();

			Point agentLocation = playerDTO.agentLocation;
			if (agentLocation != null)
			{
				Agent agent = gameState.spawnAgent(playerDTO.agentLocation, player.getKey());
				((UpdatableAgent) agent).setLastMove(playerDTO.agentLastMove);
			}
		}

		for (int x = 0; x < dto.gamePlanWidth; x++)
		{
			for (int y = 0; y < dto.gamePlanHeight; y++)
			{
				Point position = new Point(x, y);
				gameState.setResourcesOnField(position, dto.fieldResources.get(x).get(y));

				BombDTO bombDTO = dto.fieldBombs.get(x).get(y);
				if (bombDTO != null)
					gameState.setBombOnField(position, new Bomb(playerIdMap.get(bombDTO.ownerId), bombDTO.countdown));
			}
		}

		for (Map.Entry<Integer, PlayerDTO> player : dto.players.entrySet())
			gameState.setPlayerResources(player.getKey(), player.getValue().resources);

		return gameState;
	}

	public static GamePlan createGamePlanFromDto(GameStateDTO dto)
	{
		Map<Integer, Point> startingPositions = dto.players.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().homeBaseLocation));

		return new StandardGamePlan(
				dto.gamePlanWidth,
				dto.gamePlanHeight,
				startingPositions,
				dto.remainingRounds + dto.currentRound - 1);
	}

	public static List<Player> createPlayerListFromDto(GameStateDTO dto)
	{
		return dto.players.keySet().stream()
				.map(playerId -> new Player(playerId, dto.players.get(playerId).name))
				.collect(Collectors.toList());
	}

	public static class GameStateCreationException extends RuntimeException
	{
		public GameStateCreationException(String message)
		{
			super(message);
		}
	}
}
