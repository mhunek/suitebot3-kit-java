package suitebot3.server;

public interface SimpleRequestHandler
{
	String processRequest(String request);
}
