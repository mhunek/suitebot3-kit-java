package suitebot3.game;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class StandardGamePlanTest
{
	@Test(expected = IllegalStateException.class)
	public void startingPositionOutsideOfThePlanShouldThrowException() throws Exception
	{
		new StandardGamePlan(3, 3,
				ImmutableMap.of(1, new Point(3, 2)));
	}

	@Test
	public void testCreateFromOtherGamePlan() throws Exception
	{
		GamePlan plan1 = new StandardGamePlan(3, 4, ImmutableMap.of(1, new Point(1, 1), 2, new Point(2, 2)), 10);

		GamePlan plan2 = new StandardGamePlan(plan1);

		assertThat(plan1.getWidth(), is(plan2.getWidth()));
		assertThat(plan1.getHeight(), is(plan2.getHeight()));
		assertThat(plan1.getHomeBasePositions(), equalTo(plan2.getHomeBasePositions()));
		assertThat(plan1.getMaxRounds(), equalTo(plan2.getMaxRounds()));
	}
}