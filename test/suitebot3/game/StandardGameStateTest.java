package suitebot3.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import suitebot3.ai.HelperClasses;
import suitebot3.ai.SampleBotAi;
import suitebot3.game.dto.GameStateDTO;
import suitebot3.game.util.VisualisationUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class StandardGameStateTest
{
	public static final Player PLAYER1 = new Player(1, "Player 1");
	public static final Player PLAYER2 = new Player(2, "Player 2");

	private static final Point PLAYER1_START_12_X_12 = new Point(11, 5);
	private static final Point PLAYER2_START_12_X_12 = new Point(11, 3);

	private static final Map<Integer, Point> STARTING_POSITIONS_12_X_12 = ImmutableMap.of(1, PLAYER1_START_12_X_12, 2, PLAYER2_START_12_X_12);

	public static final GamePlan GAME_PLAN_12_X_12 = new StandardGamePlan(12, 12, STARTING_POSITIONS_12_X_12);
	private GameState gameState;

	@Before
	public void setUp()
	{
		gameState = new StandardGameState(GAME_PLAN_12_X_12, Arrays.asList(PLAYER1, PLAYER2), true);
	}

	@Test(expected = IllegalStateException.class)
	public void mismatchBetweenStartingPositionsCountAndPlayerCountShouldThrowException()
	{
		new StandardGameState(GAME_PLAN_12_X_12, PLAYER1);
	}

	@Test
	public void testGetPlayers()
	{
		assertThat(ImmutableList.copyOf(gameState.getPlayers()), equalTo(ImmutableList.of(PLAYER1, PLAYER2)));
	}

	@Test
	public void isAgentAndBaseOnEmptyField_returnFalse()
	{
		FieldState field00 = gameState.getField(new Point(0, 0));
		assertThat(field00.getAgent(), is(nullValue()));
		assertThat(field00.getBase(), is(nullValue()));
	}

	@Test
	public void isAgentOnFieldOnOccupiedField_returnsTrue()
	{
		assertThat(gameState.getField(PLAYER1_START_12_X_12).getAgent(), is(not(nullValue())));
	}

	@Test(expected = GamePlan.PointOutsideOfGamePlan.class)
	public void getFieldOutsidePlan_shouldThrow()
	{
		gameState.getField(new Point(-1, 0));
	}

	@Test
	public void playerStatesShouldBeInitializedFromGamePlan()
	{
		assertThat(gameState.getAgentOfPlayer(PLAYER1.id).getPosition(), equalTo(PLAYER1_START_12_X_12));
		assertThat(gameState.getAgentOfPlayer(PLAYER2.id).getPosition(), equalTo(PLAYER2_START_12_X_12));
	}

	@Test(expected = IllegalStateException.class)
	public void duplicatePlayerIdShouldThrowException()
	{
		new StandardGameState(GAME_PLAN_12_X_12, PLAYER1, new Player(PLAYER1.id, "Duplicate ID player"));
	}

	@Test
	public void testDtoGamePlan()
	{
		GameStateDTO dto = GameStateCreator.fromString("A .\n   ").toDto();
		assertThat(dto.gamePlanWidth, is(3));
		assertThat(dto.gamePlanHeight, is(2));
	}

	@Test
	public void testDtoPlayerHomeBase()
	{
		GameStateDTO dto = GameStateCreator.fromString("A  \n B ").toDto();
		assertThat(dto.players.size(), is(2));
		assertThat(dto.players.get(1).homeBaseLocation, is(new Point(0, 0)));
		assertThat(dto.players.get(2).homeBaseLocation, is(new Point(1, 1)));
	}

	@Test
	public void testDtoFieldResources()
	{
		GameStateDTO dto = GameStateCreator.fromString(" . \n  .").toDto();
		assertThat(dto.fieldResources.get(0).get(0), is(VisualisationUtil.getResources(' ')));
		assertThat(dto.fieldResources.get(1).get(0), is(VisualisationUtil.getResources('.')));
		assertThat(dto.fieldResources.get(2).get(1), is(VisualisationUtil.getResources('.')));
	}

	@Test
	public void testSpawnAgent()
	{
		final Point position = new Point(1, 0);
		StandardGameState gameState = GameStateCreator.fromString("A ");
		gameState.killAgent(gameState.getAgentOfPlayer(1));
		Agent agent = gameState.spawnAgent(position, 1);

		assertThat(agent.getPosition(), is(position));
		assertThat(agent.getPlayer().id, is(1));
		assertThat(agent.getLastMove(), is(nullValue()));

		assertThat(gameState.getField(position).getAgent(), is(agent));
	}

	@Test(expected = IllegalStateException.class)
	public void testOnlyOneAgentIsAllowed()
	{
		final Point position = new Point(1, 0);
		StandardGameState gameState = GameStateCreator.fromString("A ");
		Agent agent = gameState.spawnAgent(position, 1);
	}

	@Test
	public void testMoreWaysHowToGetAgent()
	{
		StandardGameState gameState = GameStateCreator.fromString("A");
		Agent agentFromPlayer = gameState.getPlayerState(1).getAgent();
		Agent agentFromField = gameState.getField(new Point(0, 0)).getAgent();

		assertThat(agentFromPlayer, is(agentFromField));
		assertThat(agentFromPlayer.getPosition(), is(new Point(0, 0)));
		assertThat(agentFromField.getPlayer().id, is(1));
	}

	@Test
	public void testKillingAgent()
	{
		StandardGameState gameState = GameStateCreator.fromString("A");
		gameState.killAgent(gameState.getAgentOfPlayer(1));

		assertThat(gameState.getPlayerState(1).isAgentAlive(), is(false));
		assertThat(gameState.getPlayerState(1).getAgent(), is(nullValue()));
		assertThat(gameState.getField(new Point(0, 0)).getAgent(), is(nullValue()));
	}

	@Test
	public void testMovingAnt()
	{
		StandardGameState gameState = GameStateCreator.fromString("A ");
		Agent agent = gameState.getField(new Point(0, 0)).getAgent();
		gameState.moveAgent(agent, new Point(1, 0));

		assertThat(agent.getPosition(), is(new Point(1, 0)));
		assertThat(gameState.getField(new Point(0, 0)).getAgent(), is(nullValue()));
		assertThat(gameState.getField(new Point(1, 0)).getAgent(), is(agent));
	}

	@Test
	public void testSetResources()
	{
		StandardGameState gameState = GameStateCreator.fromString("A");
		gameState.setPlayerResources(1, 500);

		assertThat(gameState.getPlayerResources(1), is(500));
	}

	@Test
	public void testSetResourcesOnField()
	{
		StandardGameState gameState = GameStateCreator.fromString("A.");

		gameState.setResourcesOnField(new Point(1, 0), 1);
		assertThat(gameState.getField(new Point(1, 0)).getResourceCount(), is(1));
	}

	@Test(expected = IllegalStateException.class)
	public void settingMoreResourcesThanAllowed_throws()
	{
		StandardGameState gameState = GameStateCreator.fromString("A.");

		gameState.setResourcesOnField(new Point(1, 0), 500000);
		assertThat(gameState.getField(new Point(1, 0)).getResourceCount(), is(GameConstants.FIELD_MAX_RESOURCES));
	}

	@Test
	public void testCopyConstructor()
	{
		StandardGameState original = createGameStateWithAllFeatures();
		StandardGameState copy = new StandardGameState(original);
		assertThat(copy, equalTo(original));
	}

	@Test
	public void testDtoTransfer()
	{
		StandardGameState original = createGameStateWithAllFeatures();
		GameStateDTO dto = original.toDto();
		StandardGameState copy = GameStateCreator.fromDto(dto);
		assertThat(copy, equalTo(original));
	}

	private StandardGameState createGameStateWithAllFeatures()
	{
		StandardGameState gameState = GameStateCreator.fromString("A. ");
		Player player = gameState.getPlayers().get(0);
		gameState.getFieldUpdatable(new Point(2,0)).setBomb(new Bomb(player, 3));

		return gameState;
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void Test() {
		StandardGameState gameState = GameStateCreator.fromString("A...\n....\n....\n....\n....\n....\n....\n....\n....\n..B.\n....");
		//gameState.setBombOnField(new Point(2, 0), new Bomb(null, 4));
		boolean res =HelperClasses.isSafePlaceFromBomb(gameState, new Point(0, 0));
		Assert.assertTrue(res);
		
		gameState = GameStateCreator.fromString("A. ");
		gameState.setBombOnField(new Point(1, 0), new Bomb(null, 1));
		res =HelperClasses.isSafePlaceFromBomb(gameState, new Point(0, 0));
		Assert.assertFalse(res);
		res =HelperClasses.ImminentThread(gameState, new Point(0, 0));
		Assert.assertTrue(res);
		
		gameState = GameStateCreator.fromString("A...\n....\n....\n....\n....\n....\n....\n....\n....\n..B.\n....");
		gameState.setBombOnField(new Point(2,3), new Bomb(null, 1));
		res =HelperClasses.ImminentThread(gameState, new Point(0, 0));
		Assert.assertFalse(res);
		
		
		gameState = GameStateCreator.fromString("A...\n....\n....\n....\n....\n....\n....\n....\n....\n..B.\n....");
		gameState.setBombOnField(new Point(1, 0), new Bomb(null, 4));
		 HashSet<Point> points  =HelperClasses.findSafePointToRun(gameState, new Point(0, 0));
		//Assert.assertTrue(res);
		
		gameState = GameStateCreator.fromString("A...\n....\n....\n....\n....\n....\n....\n....\n....\n..B.\n....");
		gameState.setBombOnField(new Point(1, 0), new Bomb(null, 4));
		ArrayList<Action[]> all = new ArrayList<Action[]>() ;
		all.add(SampleBotAi.ACTIONS1);
		all.add(SampleBotAi.ACTIONS2);
		Action[] result  =HelperClasses.chooseBestMovepatern(gameState,all,new Point(0, 0) );
		//Assert.assertTrue(res);
		
		
		gameState = GameStateCreator.fromString("A.......\n........\n........\n........\n........\n........\n........\n........\n........\n.....B..\n........");
		int score =HelperClasses.bombScore(gameState, new Point(3, 3));
		
		
		gameState = GameStateCreator.fromString("A.......\n........\n........\n........\n........\n........\n........\n........\n........\n.....B..\n........");
		gameState.setBombOnField(new Point(1, 0), new Bomb(null, 4));
		Action resultA = HelperClasses.chooseSafeAround(gameState,new Point(2, 2) );
		Assert.assertTrue(res);
	}
	
	@Test
	public void Test2() {
		
		 Point res = SampleBotAi.FuturePoint(SampleBotAi.ACTIONS1);
		 res.toString();
	}
	
}
