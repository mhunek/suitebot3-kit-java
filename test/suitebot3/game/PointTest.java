package suitebot3.game;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class PointTest
{
	@Test
	public void xHasTheValueSetInTheConstructor() throws Exception
	{
		int x = 13;
		Point point = new Point(x, 17);

		assertThat(point.x, is(x));
	}

	@Test
	public void yHasTheValueSetInTheConstructor() throws Exception
	{
		int y = 19;
		Point point = new Point(11, y);

		assertThat(point.y, is(y));
	}

	@Test
	public void samePointsEqual() throws Exception
	{
		assertThat(new Point(1, 1), equalTo(new Point(1, 1)));
	}

	@Test
	public void differentPointsDoNotEqual() throws Exception
	{
		assertThat(new Point(1, 1), not(equalTo(new Point(1, 2))));
		assertThat(new Point(1, 1), not(equalTo(new Point(2, 1))));
	}
}