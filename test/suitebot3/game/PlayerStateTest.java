package suitebot3.game;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class PlayerStateTest
{

	private Player player;
	private PlayerState playerState;

	@Before
	public void setUp()
	{
		player = new Player(1, "1");
		playerState = new PlayerState(player);
		playerState.spawnAgent(new Point(1, 2));
	}

	@Test
	public void testGetAgent()
	{
		assertThat(playerState.getAgent().getPosition(), equalTo(new Point(1, 2)));
	}

	@Test
	public void testGetAgentPositions()
	{
		assertThat(playerState.getAgent().getPosition(), equalTo(new Point(1, 2)));
	}

	@Test
	public void testKillAgent ()
	{
		playerState.killAgent();
		assertThat(playerState.getAgent(), is(nullValue()));
	}

	@Test
	public void testKillAgent2()
	{
		playerState.getUpdatableAgent().kill();
		assertThat(playerState.getAgent(), is(nullValue()));
	}

	@Test
	public void testGetPlayer()
	{
		assertThat(playerState.getPlayer(), is(player));
		assertThat(playerState.getAgent().getPlayer(), is(player));
	}

	@Test
	public void testSetPosition()
	{
		playerState.getUpdatableAgent().setPosition(new Point(5, 10));
		assertThat(playerState.getUpdatableAgent().getPosition(), is(new Point(5, 10)));
	}
}