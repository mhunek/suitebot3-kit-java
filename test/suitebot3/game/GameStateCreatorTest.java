package suitebot3.game;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class GameStateCreatorTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void testCreatingFromValidString() throws Exception
	{
		String gameStateAsString = " A \n" +
								   "B  \n";
		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		assertThat(gameState.getGamePlan().getWidth(), is(3));
		assertThat(gameState.getGamePlan().getHeight(), is(2));

		assertThat(gameState.getField(new Point(2, 0)).getAgent(), is(nullValue()));
		assertThat(gameState.getField(new Point(1, 1)).getAgent(), is(nullValue()));

		assertThat(gameState.getPlayers().size(), is(2));

		assertThat(gameState.getAgentOfPlayer(1).getPosition(), equalTo(new Point(1, 0)));
		assertThat(gameState.getAgentOfPlayer(2).getPosition(), equalTo(new Point(0, 1)));
	}

	@Test
	public void invalidCharacterShouldThrowException() throws Exception
	{
		expectedException.expect(GameStateCreator.GameStateCreationException.class);
		expectedException.expectMessage("unrecognized character: &");

		GameStateCreator.fromString("  A&");
	}

	@Test
	public void nonRectangularPlanShouldThrowException() throws Exception
	{
		expectedException.expect(GameStateCreator.GameStateCreationException.class);
		expectedException.expectMessage("non-rectangular plan: line 3 width (3) is different from the line 1 width (4)");

		GameStateCreator.fromString("    \n    \n   \n    ");
	}
}
