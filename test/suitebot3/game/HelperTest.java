package suitebot3.game;
import com.google.common.collect.ImmutableMap;

import suitebot3.ai.HelperClasses;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class HelperTest {

	@Test
	public void testCreateFromOtherGamePlan() throws Exception
	{
		GamePlan plan1 = new StandardGamePlan(10, 10, ImmutableMap.of(1, new Point(1, 1), 2, new Point(2, 2)), 10);

	
		Point p1 = new Point(0, 0); 
		Point p2 = new Point(1,1);
		Point p3 = new Point(9, 0);
		assertThat(0,is(p1.absolutDistance()));
		assertThat(2,is( p2.absolutDistance()));

		Point res = HelperClasses.countDistanceBetweenPoints(plan1, p1, p2);
		
		Point res3 = HelperClasses.countDistanceBetweenPoints(plan1, p1, p3);
		
		assertThat( 2,is( res.absolutDistance()));
		assertThat( new Point(1,1),is( res));
		
		assertThat( 1,is( res3.absolutDistance()));
		assertThat( new Point(1,0),is( res3));

		
		Point res2 = HelperClasses.direction(plan1, p2, p1);
		assertThat( new Point(-1,-1),is( res2));

		
		
		Point res4 = HelperClasses.direction(plan1, p3, p1);
		assertThat( new Point(1,0),is( res4));
				
	}
	
}
