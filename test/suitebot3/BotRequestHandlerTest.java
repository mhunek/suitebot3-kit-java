package suitebot3;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import suitebot3.ai.BotAi;
import suitebot3.game.Action;
import suitebot3.game.GameState;
import suitebot3.game.GameStateCreator;
import suitebot3.game.dto.GameStateDTO;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BotRequestHandlerTest
{
    private BotRequestHandler requestHandler;

    @Before
    public void setUp()
    {
        requestHandler = new BotRequestHandler(gameSetup -> new DummyBotAi());
    }

    @Test
    public void onSetupMessage_initializeShouldBeCalled()
    {
        String aiMove = requestHandler.processRequest(createGameStateString(1));
        assertThat(aiMove, is("L"));
    }

    @Test
    public void onMovesMessage_makeMoveShouldBeCalled()
    {
        requestHandler.processRequest(createGameStateString(1));
        String aiMove = requestHandler.processRequest(createGameStateString(2));
        assertThat(aiMove, is("R"));
    }

    @Test
    public void onInvalidRequest_noAiMethodsShouldBeCalled()
    {
        requestHandler.setSuppressErrorLogging(true);
        requestHandler.processRequest("invalid request");
    }

    private String createGameStateString(int currentRound) {
        GameStateDTO dto = GameStateCreator.fromString("A...\n....\n..B.\n....").toDto();
        dto.currentRound = currentRound;
        dto.remainingRounds = 150 - currentRound;

        return new Gson().toJson(dto);
    }

    class DummyBotAi implements BotAi
    {
        @Override
        public Action makeMove(GameState gameState)
        {
            if (gameState.getCurrentRound() == 1)
                return Action.LEFT;
            else if (gameState.getCurrentRound() == 2)
                return Action.RIGHT;
            else
                throw new RuntimeException("unexpected round");
        }

    }
}